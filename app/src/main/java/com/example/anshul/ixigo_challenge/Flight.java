package com.example.anshul.ixigo_challenge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;
import java.util.List;

/**
 * Created by anshul on 4/2/17.
 */
public class Flight implements Comparable<Flight> {
    @SerializedName("originCode")
    @Expose
    private String originCode;
    @SerializedName("destinationCode")
    @Expose
    private String destinationCode;
    @SerializedName("departureTime")
    @Expose
    private Long departureTime;
    @SerializedName("arrivalTime")
    @Expose
    private Long arrivalTime;
    @SerializedName("fares")
    @Expose
    private List<Fare> fares = null;
    @SerializedName("airlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("class")
    @Expose
    private String _class;

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Long departureTime) {
        this.departureTime = departureTime;
    }

    public Long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(List<Fare> fares) {
        this.fares = fares;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

 /*   @Override
    public int compare(Object o1, Object o2) {
        List<Fare> list1 = (List<Fare>)o1;
        List<Fare> list2 = (List<Fare>)o2;
        int tempFare=0;
        int minFare=list1.get(0).getFare();
        for(int i=0;i<list1.size();i++){
             tempFare = list1.get(i).getFare();
            if(tempFare<minFare){
                minFare=tempFare;
            }
        }

        tempFare=0;
        int minFare2=list2.get(0).getFare();
        for(int i=0;i<list2.size();i++){
            tempFare = list2.get(i).getFare();
            if(tempFare<minFare2){
                minFare2=tempFare;
            }
        }

        if(minFare==minFare2){
            return 0;
        }else if(minFare>minFare2){
            return 1;
        }else {
            return -1;
        }
    }*/

    @Override
    public int compareTo(Flight o) {
        List<Fare> list1 = o.getFares();

        List<Fare> list2 = fares;
        int tempFare=0;
        int minFare=list1.get(0).getFare();
        for(int i=0;i<list1.size();i++){
            tempFare = list1.get(i).getFare();
            if(tempFare<minFare){
                minFare=tempFare;
            }
        }

        tempFare=0;
        int minFare2=list2.get(0).getFare();
        for(int i=0;i<list2.size();i++){
            tempFare = list2.get(i).getFare();
            if(tempFare<minFare2){
                minFare2=tempFare;
            }
        }

        if(minFare==minFare2){
            return 0;
        }else if(minFare>minFare2){
            return -1;
        }else {
            return 1;
        }


    }
}
