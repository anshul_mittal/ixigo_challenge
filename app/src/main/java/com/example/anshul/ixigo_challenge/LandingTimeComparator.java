package com.example.anshul.ixigo_challenge;

import java.util.Comparator;

/**
 * Created by anshul on 4/3/17.
 */
public class LandingTimeComparator implements Comparator {
    //sorting the list based on landing time
    @Override
    public int compare(Object o1, Object o2) {
        Flight flight1 = (Flight)o1;
        Flight flight2 = (Flight)o2;


        if(flight1.getArrivalTime()==flight2.getArrivalTime()){
            return 0;
        }else if(flight1.getArrivalTime()>flight2.getArrivalTime()){
            return 1;
        }else {
            return -1;
        }
    }
}
