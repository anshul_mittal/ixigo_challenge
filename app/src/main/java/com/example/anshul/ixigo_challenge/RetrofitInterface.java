package com.example.anshul.ixigo_challenge;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by anshul on 4/1/17.
 */
public interface RetrofitInterface {
    //retrofit api
    @GET
    Call<FlightResponse> getData(@Url String url);
}
