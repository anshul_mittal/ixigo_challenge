package com.example.anshul.ixigo_challenge;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by anshul on 4/1/17.
 */
public class CustomAdapter extends BaseAdapter {
    Context context;
    private LayoutInflater mInflator;
    List<Flight> list;
    Appendix appendix;
    @Override
    public int getCount() {
        return list.size();
    }
CustomAdapter(Context context, List<Flight> list, Appendix appendix)
{
    this.context=context;
    //this.smsList=smsList;
    this.list=list;
    this.appendix=appendix;
    mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
}
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView==null)
        {
            convertView=mInflator.inflate(R.layout.flight_list_item,null);
            holder = new ViewHolder();
            holder.flight_name=(TextView) convertView.findViewById(R.id.flight_name);
            holder.flight_class=(TextView)convertView.findViewById(R.id.flight_class);
            holder.flight_begining=(TextView)convertView.findViewById(R.id.flight_begining);
            holder.flight_end=(TextView)convertView.findViewById(R.id.flight_end);
            holder.flight_duration=(TextView)convertView.findViewById(R.id.flight_duration);
            holder.flight_provider=(TextView)convertView.findViewById(R.id.flight_provider);
            holder.fare=(TextView)convertView.findViewById(R.id.fare);
            holder.flight_provider2=(TextView)convertView.findViewById(R.id.flight_provider2);
            holder.fare2=(TextView)convertView.findViewById(R.id.fare2);
            holder.flight_provider3=(TextView)convertView.findViewById(R.id.flight_provider3);
            holder.fare3=(TextView)convertView.findViewById(R.id.fare3);
            holder.flight_provider4=(TextView)convertView.findViewById(R.id.flight_provider4);
            holder.fare4=(TextView)convertView.findViewById(R.id.fare4);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.flight_name.setText(appendix.getAirlines().get(list.get(position).getAirlineCode())+"");
        holder.flight_duration.setText(getDuration(list.get(position).getArrivalTime(),list.get(position).getDepartureTime())+" Duration");
        holder.flight_begining.setText(formatDate(list.get(position).getDepartureTime())+"");
        holder.flight_end.setText(formatDate(list.get(position).getArrivalTime())+"");
        holder.flight_class.setText((list.get(position).getClass_()+" Class"));
        //if (list.get(position).getFares().)
        //appendix.getString("providers");

        switch (list.get(position).getFares().size())
        { //get the size of fares array
            case 1:

                    //Log.e("provider",appendix.getProviders().getString(list.get(position).getFares().get(0).getProviderId()+""));
                    holder.flight_provider.setText( appendix.getProviders().get(list.get(position).getFares().get(0).getProviderId()+"")+"");

                holder.fare.setText("Rs."+list.get(position).getFares().get(0).getFare()+"");
                holder.flight_provider2.setVisibility(View.GONE);
                holder.fare2.setVisibility(View.GONE);
                holder.flight_provider3.setVisibility(View.GONE);
                holder.fare3.setVisibility(View.GONE);
                holder.flight_provider4.setVisibility(View.GONE);
                holder.fare4.setVisibility(View.GONE);
                break;
            case 2:
                holder.flight_provider.setText(appendix.getProviders().get(list.get(position).getFares().get(0).getProviderId()+"")+"");
                holder.flight_provider2.setText(appendix.getProviders().get(list.get(position).getFares().get(1).getProviderId()+"")+"");
                holder.fare.setText("Rs."+list.get(position).getFares().get(0).getFare()+"");
                holder.fare2.setText("Rs."+list.get(position).getFares().get(1).getFare()+"");
                holder.flight_provider2.setVisibility(View.VISIBLE);
                holder.fare2.setVisibility(View.VISIBLE);
                holder.flight_provider3.setVisibility(View.GONE);
                holder.fare3.setVisibility(View.GONE);
                holder.flight_provider4.setVisibility(View.GONE);
                holder.fare4.setVisibility(View.GONE);
                break;
            case 3:
                holder.flight_provider.setText(appendix.getProviders().get(list.get(position).getFares().get(0).getProviderId()+"")+"");
                holder.flight_provider2.setText(appendix.getProviders().get(list.get(position).getFares().get(1).getProviderId()+"")+"");
                holder.flight_provider3.setText(appendix.getProviders().get(list.get(position).getFares().get(2).getProviderId()+"")+"");
                holder.fare.setText("Rs."+list.get(position).getFares().get(0).getFare()+"");
                holder.fare2.setText("Rs."+list.get(position).getFares().get(1).getFare()+"");
                holder.fare3.setText("Rs."+list.get(position).getFares().get(2).getFare()+"");
                holder.flight_provider2.setVisibility(View.VISIBLE);
                holder.fare2.setVisibility(View.VISIBLE);
                holder.flight_provider3.setVisibility(View.VISIBLE);
                holder.fare3.setVisibility(View.VISIBLE);
                holder.flight_provider4.setVisibility(View.GONE);
                holder.fare4.setVisibility(View.GONE);
                break;
            case 4:
                holder.flight_provider.setText(appendix.getProviders().get(list.get(position).getFares().get(0).getProviderId()+"")+"");
                holder.flight_provider2.setText(appendix.getProviders().get(list.get(position).getFares().get(1).getProviderId()+"")+"");
                holder.flight_provider3.setText(appendix.getProviders().get(list.get(position).getFares().get(2).getProviderId()+"")+"");
                holder.flight_provider4.setText(appendix.getProviders().get(list.get(position).getFares().get(3).getProviderId()+"")+"");
                holder.fare.setText("Rs."+list.get(position).getFares().get(0).getFare()+"");
                holder.fare2.setText("Rs."+list.get(position).getFares().get(1).getFare()+"");
                holder.fare3.setText("Rs."+list.get(position).getFares().get(2).getFare()+"");
                holder.fare4.setText("Rs."+list.get(position).getFares().get(3).getFare()+"");
                holder.flight_provider2.setVisibility(View.VISIBLE);
                holder.fare2.setVisibility(View.VISIBLE);
                holder.flight_provider3.setVisibility(View.VISIBLE);
                holder.fare3.setVisibility(View.VISIBLE);
                holder.flight_provider4.setVisibility(View.VISIBLE);
                holder.fare4.setVisibility(View.VISIBLE);
                break;
        }
        return convertView;
    }
    class ViewHolder{
        TextView flight_name;
        TextView flight_class;
        TextView flight_begining;
        TextView flight_end;
        TextView flight_duration;
        TextView flight_provider;
        TextView fare;
        TextView flight_provider2;
        TextView fare2;
        TextView flight_provider3;
        TextView fare3;
        TextView flight_provider4;
        TextView fare4;


    }


    private String formatDate(long milliseconds) /* This is your topStory.getTime()*1000 */ {
        DateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        TimeZone tz = TimeZone.getDefault();
        sdf.setTimeZone(tz);
        return sdf.format(calendar.getTime());
    }
    private String getDuration(long timestamp1, long timestamp2)
    {long milliseconds = timestamp1- timestamp2;
        int seconds = (int) milliseconds / 1000;

        // calculate hours minutes and seconds
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
       // seconds = (seconds % 3600) % 60;
        return hours+":"+minutes+ " Hours";

    }
}
